public class Fish {
    private String name;
    private int length;
    private String level;
    private int price;

    public Fish(String name,int length,String level,int price){
        this.name = name;
        this.length = length;
        this.level = level;
        this.price = price;
    }
    
    public void print(){
        System.out.println("name : "+name);
        System.out.println("length : "+length+" cm");
        System.out.println("level : "+level);
        System.out.println("price : "+price +" $");
        System.out.println("---------------------------");
    }
}
