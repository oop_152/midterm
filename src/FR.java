public class FR {
    private String name;
    private int success;
    private int rare_fish;
    private int uncommon_fish;
    private int total = 0;

    public FR(String name,int success,int rare_fish,int uncommon_fish){
        this.name = name;
        this.success = success;
        this.rare_fish = rare_fish;
        this.uncommon_fish = uncommon_fish;
    }

    public void printStatus(){
        System.out.println("name : "+name);
        System.out.println("Increases the success chance by "+success +" %");
        System.out.println("Increases the chance to get rare fish by "+rare_fish +" %");
        System.out.println("Increases the chance to get uncommon fish by "+uncommon_fish +" %");
    }

    public int total(int f1,int f2,int f3){
        total = total + f1 + f2 + f3;
        System.out.println("The total price of fish : "+total+" $");
        return total;
    }

    public void printAllfish(String f1,String f2,String f3){
        System.out.println("fish from fishing : "+f1+", "+f2+", "+f3);
    }
}
