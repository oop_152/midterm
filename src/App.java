public class App {
    public static void main(String[] args) throws Exception {
        FR pro = new FR("Pro Rod", 80, 40, 100);        // เบ็ดราคา 15000
        pro.printStatus();
        pro.printAllfish("Queen Angelfish : 17 $", "Crowned Mackerel : 38 $", "Rockfish : 24 $");
        pro.total(17,38,24);
        System.out.println("-------------------------------------------------");
        FR pocket = new FR("Pocket Rod", 20, 0, 75);    // เบ็ดราคา 1500
        pocket.printStatus();
        pocket.printAllfish("Pacificsaury : 84 $", "Rockfish : 29 $", "Scorpionfish : 18 $");
        pocket.total(84, 18, 29);
        System.out.println("-------------------------------------------------");
        FR wooden = new FR("Wooden Rod", 0, 0, 0);      // เบ็ดราคา 450
        wooden.printStatus();
        wooden.printAllfish("Rockfish : 24 $", "Purple Dottyback : 160 $", "Starfish : 20 $");
        wooden.total(24, 160, 20);
        System.out.println("-------------------------------------------------");
        System.out.println("level and price of fish");
        Fish f1 = new Fish("Purple Dottyback", 6, "Uncommon", 160);
        Fish f2 = new Fish("Starfish", 10, "Common", 20);
        Fish f3 = new Fish("Piranha", 16, "rare", 166);
        Fish f4 = new Fish("Hammerhead Shark", 331, "Epic", 451);
        f2.print();
        f1.print();
        f3.print();
        f4.print();
    }
}
